use enum_iterator::{all, Sequence};
use int_enum::IntEnum;

#[repr(u32)]
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, IntEnum, Sequence)]
pub enum ResistorColor {
    Green = 5,
    Brown = 1,
    Black = 0,
    Orange = 3,
    Violet = 7,
    Red = 2,
    Grey = 8,
    Yellow = 4,
    Blue = 6,
    White = 9,
}

pub fn color_to_value(_color: ResistorColor) -> u32 {
    _color as u32
}

pub fn value_to_color_string(value: u32) -> String {
    match ResistorColor::from_int(value) {
        Ok(color) => format!("{:?}", color),
        Err(_) => format!("value out of range"),
    }
}

pub fn colors() -> Vec<ResistorColor> {
    let mut colors: Vec<ResistorColor> = all::<ResistorColor>().collect();
    colors.sort();
    colors
}
