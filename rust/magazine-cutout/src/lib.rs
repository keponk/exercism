// This stub file contains items that aren't used yet; feel free to remove this module attribute
// to enable stricter warnings.
#![allow(unused)]

use std::collections::HashMap;

pub fn can_construct_note(magazine: &[&str], note: &[&str]) -> bool {
    let mut counter = HashMap::new();

    magazine
        .iter()
        .for_each(|word| *counter.entry(word).or_insert(0) += 1);
        
    note.iter().all(|word| match counter.get(word) {
        Some(&count) if count > 0 => {
            *counter.entry(word).or_default() -= 1;
            true
        }
        _ => false,
    })
}
