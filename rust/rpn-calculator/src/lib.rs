#[derive(Debug)]
pub enum CalculatorInput {
    Add,
    Subtract,
    Multiply,
    Divide,
    Value(i32),
}

pub fn evaluate(inputs: &[CalculatorInput]) -> Option<i32> {
    if inputs.is_empty() {
        return None;
    }

    let mut stack = vec![];

    for input in inputs {
        match input {
            CalculatorInput::Value(x) => stack.push(*x),
            _ => {
                let result = match (input, stack.pop(), stack.pop()) {
                    (CalculatorInput::Add, Some(x), Some(y)) => x + y,
                    (CalculatorInput::Multiply, Some(x), Some(y)) => x * y,
                    (CalculatorInput::Subtract, Some(x), Some(y)) => y - x,
                    (CalculatorInput::Divide, Some(x), Some(y)) => y / x,
                    _ => return None,
                };
                stack.push(result)
            }
        }
    }

    (stack.len() == 1).then(|| stack[0])
}
