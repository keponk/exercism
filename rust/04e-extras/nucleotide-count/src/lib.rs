// use std::collections::hash_map::Entry::{Occupied, Vacant};
use std::collections::HashMap;

pub fn count(nucleotide: char, dna: &str) -> Result<usize, char> {
    match nucleotide {
        'A' | 'C' | 'G' | 'T' => match nucleotide_counts(dna) {
            Ok(hashmap) => Ok(*hashmap.get(&nucleotide).unwrap()),
            Err(letter) => Err(letter),
        },
        _ => Err(nucleotide),
    }
}

pub fn nucleotide_counts(dna: &str) -> Result<HashMap<char, usize>, char> {
    let mut a_count = 0;
    let mut c_count = 0;
    let mut g_count = 0;
    let mut t_count = 0;

    for ch in dna.chars() {
        match ch {
            'A' => a_count += 1,
            'C' => c_count += 1,
            'G' => g_count += 1,
            'T' => t_count += 1,
            _ => return Err(ch),
        }
    }

    let mut hm = HashMap::with_capacity(4);
    hm.insert('A', a_count);
    hm.insert('C', c_count);
    hm.insert('G', g_count);
    hm.insert('T', t_count);
    Ok(hm)
}
