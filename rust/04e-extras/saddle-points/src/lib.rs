pub fn find_saddle_points(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    let mut result: Vec<(usize, usize)> = Vec::new();
    for (row_index, row_data) in input.iter().enumerate() {
        if row_data.is_empty() {
            break;
        }
        let max_in_row_value = row_data.iter().max().unwrap();
        for (col_index, col_data) in row_data.iter().enumerate() {
            if col_data == max_in_row_value
                && *col_data == input.iter().map(|r| r[col_index]).min().unwrap()
            {
                result.push((row_index, col_index));
            }
        }
    }
    result
}
