#[derive(Clone)]
pub struct PascalsTriangle {
    value: Vec<Vec<u32>>,
}

impl PascalsTriangle {
    pub fn new(row_count: u32) -> Self {
        let factorial = |num: u32| -> u32 { (1..=num).product() };
        let calc_cell = |row, col| factorial(row) / (factorial(col) * factorial(row - col));
        PascalsTriangle {
            value: (0..row_count)
                .map(|row| {
                    (0..=row)
                        .map(|col| {
                            if row > 1 && col > 0 && row != col {
                                calc_cell(row, col)
                            } else {
                                1
                            }
                        })
                        .collect::<Vec<u32>>()
                })
                .collect(),
        }
    }

    pub fn rows(&self) -> Vec<Vec<u32>> {
        self.value.clone()
    }
}
