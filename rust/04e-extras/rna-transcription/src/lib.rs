#[derive(Debug, PartialEq)]
pub struct Dna(String);

#[derive(Debug, PartialEq)]
pub struct Rna(String);

fn validate(seq: &str, valid_nucleots: &str) -> Result<String, usize> {
    match seq.chars().position(|c| !valid_nucleots.contains(c)) {
        Some(index) => Err(index),
        None => Ok(seq.to_string()),
    }
}

impl Dna {
    pub fn new(dna: &str) -> Result<Dna, usize> {
        Ok(Dna(validate(dna, "GCTA")?))
    }

    pub fn into_rna(self) -> Rna {
        Rna(self
            .0
            .chars()
            .map(|c| match c {
                'G' => 'C',
                'C' => 'G',
                'T' => 'A',
                'A' => 'U',
                _ => panic!("Unexpected char `{}`.", c),
            })
            .collect())
    }
}

impl Rna {
    pub fn new(rna: &str) -> Result<Rna, usize> {
        Ok(Rna(validate(rna, "CGAU")?))
    }
}
