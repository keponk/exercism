use regex::Regex;

/// Determines whether the supplied string is a valid ISBN number
pub fn is_valid_isbn(isbn: &str) -> bool {
    let re = Regex::new(r"^\d-?\d{3}-?\d{5}-?[0-9xX]{1}$").unwrap();
    re.is_match(isbn)
        && isbn
            .to_uppercase()
            .chars()
            .filter(|&c| c != '-')
            .enumerate()
            .map(|(i, val)| val.to_digit(10).unwrap_or(10) * (10 - i as u32))
            .sum::<u32>()
            % 11
            == 0
}
