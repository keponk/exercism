use std::ops::{Add, Sub};

pub struct Triangle<T> {
    sides: [T; 3],
}

impl<T> Triangle<T>
where
    T: Default + Copy + PartialEq + PartialOrd + Add<Output = T> + Sub<Output = T>,
{
    pub fn build(sides: [T; 3]) -> Option<Triangle<T>>
    where
        T: PartialOrd + Copy + Add<T, Output = T>,
    {
        let sum = sides.iter().fold(T::default(), |acc, n: &T| acc + *n);
        if sides.iter().any(|&n| n == T::default() || sum - n < n) {
            return None;
        } else {
            return Some(Triangle { sides });
        }
    }

    pub fn is_equilateral(&self) -> bool {
        self.sides[0] == self.sides[1] && self.sides[1] == self.sides[2]
    }

    pub fn is_scalene(&self) -> bool {
        self.sides[0] != self.sides[1]
            && self.sides[0] != self.sides[2]
            && self.sides[1] != self.sides[2]
    }

    pub fn is_isosceles(&self) -> bool {
        self.sides[0] == self.sides[1]
            || self.sides[0] == self.sides[2]
            || self.sides[1] == self.sides[2]
    }
}
