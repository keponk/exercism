/// Determine whether a sentence is a pangram.
pub fn is_pangram(sentence: &str) -> bool {
    // nicest solution at https://exercism.io/tracks/rust/exercises/pangram/solutions/2077b93d9b424289869f94f784da53cc
    let lowcase = sentence.to_ascii_lowercase();
    "abcdefghijklmnopqrstuvwxyz"
        .chars()
        .filter(|c| c.is_ascii_alphabetic())
        .all(|c| lowcase.contains(c))
}
