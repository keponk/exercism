use std::collections::BTreeMap;

#[allow(dead_code)]
fn imperative_solution(h: &BTreeMap<i32, Vec<char>>) -> BTreeMap<char, i32> {
    let mut out = BTreeMap::new();
    for (value, lettervec) in h {
        for letter in lettervec {
            out.entry(letter.to_ascii_lowercase()).or_insert(*value);
        }
    }
    out
}

pub fn transform(h: &BTreeMap<i32, Vec<char>>) -> BTreeMap<char, i32> {
    h.iter()
        .flat_map(|(&value, letters)| letters.iter().map(move |c| (c.to_ascii_lowercase(), value)))
        .collect()
}
