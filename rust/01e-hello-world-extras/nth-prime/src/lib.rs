// Best solution -> https://exercism.io/tracks/rust/exercises/nth-prime/solutions/65a44ef3d18a4e7e9fa85605eecb3fc8

pub fn nth(n: u32) -> u32 {
    let mut counter = 2;
    let mut primes: Vec<u32> = Vec::new();
    while primes.len() < n as usize + 1 {
        if is_prime(counter) {
            primes.push(counter)
        }
        counter += 1;
    }
    primes.pop().unwrap()
}

fn is_prime(n: u32) -> bool {
    let mut counter = n - 1;
    while counter > 1 {
        if n % counter == 0 {
            return false;
        }
        counter -= 1;
    }
    true
}
