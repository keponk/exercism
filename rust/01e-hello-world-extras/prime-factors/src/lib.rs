// Nicer solutions:
// a) https://exercism.io/tracks/rust/exercises/prime-factors/solutions/ea966d369fc94b319a24f4c8aefd9a4b
// b) https://exercism.io/tracks/rust/exercises/prime-factors/solutions/f8f68103c91548a0a268b56e81e093f4

pub fn factors(n: u64) -> Vec<u64> {
    let mut result: Vec<u64> = Vec::new();
    if n > 1 {
        for i in 2..=n {
            if n % i == 0 {
                result.push(i);
                result.append(&mut factors(n / i));
                return result;
            }
        }
    }
    result
}
