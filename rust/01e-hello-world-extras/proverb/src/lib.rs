pub fn build_proverb(list: &[&str]) -> String {
    list.iter()
        .enumerate()
        .map(|(i, x)| {
            if i + 1 == list.len() {
                return format!("And all for the want of a {}.", list[0]);
            }
            format!("For want of a {} the {} was lost.", x, list[i + 1])
        })
        .collect::<Vec<String>>()
        .join("\n")
}
