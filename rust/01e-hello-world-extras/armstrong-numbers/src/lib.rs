pub fn is_armstrong_number(num: u32) -> bool {
    let digits = num.to_string().chars().count() as u32;
    num.to_string()
        .chars()
        .map(|n| n.to_digit(10).unwrap().pow(digits))
        .sum::<u32>()
        == num
}
