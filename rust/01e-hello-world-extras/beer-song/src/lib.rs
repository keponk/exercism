pub fn verse(n: u32) -> String {
    if n == 0 {
        return "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n".to_string();
    }
    let values = match n {
        1 => ("", "", "it", "no more".to_string(), "s"),
        2 => ("s", "s", "one", (n - 1).to_string(), ""),
        _ => ("s", "s", "one", (n - 1).to_string(), "s"),
    };
    format!(
        "{} bottle{} of beer on the wall, {} bottle{} of beer.\nTake {} down and pass it around, {} bottle{} of beer on the wall.\n",
        n, 
        values.0,
        n,
        values.1,
        values.2,
        values.3,
        values.4
    )
}

pub fn sing(start: u32, end: u32) -> String {
    let mut song: String = String::new();
    for bottles in (end..=start).rev() {
        let verse = verse(bottles);
        song += &verse;
        song += "\n";
    }
    song.truncate(song.len() - 1);
    song
}
