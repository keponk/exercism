use std::collections::HashSet;

pub fn anagrams_for<'a>(word: &str, possible_anagrams: &[&'a str]) -> HashSet<&'a str> {
    possible_anagrams
        .iter()
        .filter(|pa| pa.len() == word.len() && pa.to_lowercase() != word.to_lowercase())
        .filter(|pa| {
            let mut pa_chars: Vec<char> = pa.to_lowercase().chars().collect();
            pa_chars.sort_unstable();
            let mut word_chars: Vec<char> = word.to_lowercase().chars().collect();
            word_chars.sort_unstable();
            pa_chars == word_chars
        })
        .map(|pa| *pa)
        .collect::<HashSet<&str>>()
}
