#[derive(Debug, PartialEq)]
pub enum Comparison {
    Equal,
    Sublist,
    Superlist,
    Unequal,
}

pub fn sublist<T: PartialEq>(_first_list: &[T], _second_list: &[T]) -> Comparison {
    match (_first_list.len(), _second_list.len()) {
        (0, 0) => Comparison::Equal,
        (0, _) => Comparison::Sublist,
        (_, 0) => Comparison::Superlist,
        (first, second) if first < second => {
            if _second_list.windows(first).any(|win| win == _first_list) {
                return Comparison::Sublist;
            } else {
                return Comparison::Unequal;
            }
        }
        (first, second) if first > second => {
            if _first_list.windows(second).any(|win| win == _second_list) {
                return Comparison::Superlist;
            } else {
                return Comparison::Unequal;
            }
        }
        (_, _) => {
            if _first_list == _second_list {
                Comparison::Equal
            } else {
                Comparison::Unequal
            }
        }
    }
}
