#[derive(Debug, PartialEq)]
pub enum Error {
    InvalidInputBase,
    InvalidOutputBase,
    InvalidDigit(u32),
}

pub fn convert(number: &[u32], from_base: u32, to_base: u32) -> Result<Vec<u32>, Error> {
    
    if from_base < 2 {
        return Err(Error::InvalidInputBase);
    }
    if to_base < 2 {
        return Err(Error::InvalidOutputBase);
    }

    let mut dec_num: u32 = 0;
    for (index, value) in number.iter().enumerate() {
        if *value >= from_base {
            return Err(Error::InvalidDigit(*value));
        }
        dec_num += value * from_base.pow((number.len() - index - 1) as u32);
    }
    
    if dec_num == 0 {
        return Ok(Vec::from([0]));
    }

    let mut result: Vec<u32> = Vec::new();
    while dec_num != 0 {
        result.push(dec_num % to_base);
        dec_num /= to_base;
    }
    result.reverse();
    Ok(result)
}
