/**
 * Other nice solutions:
 * 1) On opening, push closing: https://exercism.io/tracks/rust/exercises/matching-brackets/solutions/e41fcde70d8744d3a2679254e58d8ed0
 * 2) Slowly remove completed ones: https://exercism.io/tracks/rust/exercises/matching-brackets/solutions/d73b8543c86d4e9e85b602c6117c8c8f
 */

pub fn brackets_are_balanced(string: &str) -> bool {
    let is_closing_bracket = |x| matches!(x, '}' | ']' | ')');
    let match_bracket = |x| match x {
        ')' => '(',
        '}' => '{',
        ']' => '[',
        _ => panic!("Found unexpected char."),
    };
    let mut stack = Vec::new();
    for c in string.chars().filter(|c| "{}[]()".contains(&c.to_string())) {
        if is_closing_bracket(c) {
            if stack.is_empty() || stack.last().unwrap() != &match_bracket(c) {
                return false;
            } else {
                stack.pop();
            }
        } else {
            stack.push(c);
        }
    }
    stack.is_empty()
}
