use regex::Regex;

pub fn reply(message: &str) -> &str {
    let trimmed = message.trim();
    if trimmed == "" {
        return "Fine. Be that way!";
    }
    let is_question = trimmed.ends_with('?');

    let filtered_letters: String = message.chars().filter(|c| c.is_alphabetic()).collect();
    let uppercase_regex = Regex::new(r"^[A-Z]+$").expect("Invalid regex.");
    let is_upper = uppercase_regex.is_match(&filtered_letters);
    // let is_upper = filtered_letters.len() > 0
    //     && filtered_letters
    //         .chars()
    //         .filter(|c| c.is_alphabetic())
    //         .all(|c| ('A'..='Z').contains(&c));
    match (is_question, is_upper) {
        (true, true) => "Calm down, I know what I'm doing!",
        (true, false) => "Sure.",
        (false, true) => "Whoa, chill out!",
        (false, false) => "Whatever.",
    }
}
