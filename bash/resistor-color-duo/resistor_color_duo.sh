#!/usr/bin/env bash

err() {
    echo "invalid color"
    exit 1
}

get_color() {
    case ${@,,} in
    black) echo "0" ;;
    brown) echo "1" ;;
    red) echo "2" ;;
    orange) echo "3" ;;
    yellow) echo "4" ;;
    green) echo "5" ;;
    blue) echo "6" ;;
    violet) echo "7" ;;
    grey) echo "8" ;;
    white) echo "9" ;;
    *) return 1 ;;
    esac
}

a=$(get_color $1) || err
b=$(get_color $2) || err
echo "$a$b"
